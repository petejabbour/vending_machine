class VendingMachine
  attr_accessor :products, :balance

  def initialize
    # VendingMachine.new.products[:product_code] => returns the product attrs
    @products = {
      '1a' => { name: "coke", price: 0.5, inventory: 10 },
      '1b' => { name: "sprite", price: 0.5, inventory: 0 },
      '1c' => { name: "mountain dew", price: 0.75, inventory: 6 },
      '2a' => { name: "powerade", price: 1.00, inventory: 12 },
      '2b' => { name: "gatorade", price: 1.00, inventory: 4 },
      '2c' => { name: "water", price: 1.25, inventory: 2 },
    }
    @balance = 0.0
  end

  # Called when the product code is input into the machine
  def purchase(product_code)
    self.includes_product_code?(product_code)
    product = self.products[product_code]
    raise NotEnoughFundsError if product[:price] > self.balance

    product[:inventory] -= 1
    self.balance -= product[:price]

    { success: true, message: self.purchase_display_message(product) }
  end

  def purchase_display_message(product)
    message = "Dispensing #{product[:name]}"
    message += " and $#{self.display_balance} in change" if self.balance > 0
    message
  end

  # Ability to stock/restock items in the machine
  def add_inventory(product_code, new_items_count)
    self.includes_product_code?(product_code)
    self.products[product_code][:inventory] += new_items_count
  end

  def remove_inventory(product_code, new_items_count)
    self.includes_product_code?(product_code)
    self.products[product_code][:inventory] -= new_items_count
  end

  # Called when each bill or coin is input into the machine
  def add_funds(amount)
    self.balance += amount.round(2)
  end

  # cancel button that returns the user's money
  def withdraw_funds
    raise NoBalanceRemainingError if self.balance == 0.0
    message = "Dispensing $#{self.display_balance}"
    self.balance = 0.0
    { success: true, message: message }
  end

  def display_balance
    trailing_zero = ""
    trailing_zero = "0" if self.balance.to_f.to_s.split(".").last.length == 1
    self.balance.to_s + trailing_zero
  end

  def includes_product_code?(product_code)
    raise IncorrectProductCodeError if !self.products.keys.include?(product_code)
  end
end

class NotEnoughFundsError < StandardError; end
class IncorrectProductCodeError < StandardError; end
class NoBalanceRemainingError < StandardError; end
