require_relative "../src/vending_machine"

describe VendingMachine do
  subject(:vending_machine) { VendingMachine.new }

  it "initializes the products and balance" do
    expect(vending_machine.products.count > 0).to eq(true)
    expect(vending_machine.balance).to eq(0.0)
  end

  describe "instance methods" do
    before do
      @product_code = vending_machine.products.keys.first
      @product = vending_machine.products[@product_code]
      vending_machine.add_funds(1.00)
    end

    describe "#purchase" do
      it "decrements the product inventory" do
        pre_purchase_inventory_count = vending_machine.products[@product_code][:inventory]
        vending_machine.purchase(@product_code)
        expect(vending_machine.products[@product_code][:inventory]).to eq(pre_purchase_inventory_count - 1)
      end

      it "calculates the correct balance for change dispensing" do
        pre_purchase_balance = vending_machine.balance
        vending_machine.purchase(@product_code)
        expect(vending_machine.balance).to eq(pre_purchase_balance - @product[:price])
      end

      it "raises an error when not enough funds" do
        vending_machine.balance = 0.25
        expect { vending_machine.purchase(@product_code) }.to raise_error(NotEnoughFundsError)
      end

      it "raises error when incorrect product code" do
        expect { vending_machine.purchase("incorrect_code") }.to raise_error(IncorrectProductCodeError)
      end
    end

    describe "#purchase_display_message" do
      it "displays the correct message" do
        response = vending_machine.purchase("1c")
        expect(response[:message]).to eq("Dispensing mountain dew and $0.25 in change")
      end
    end

    describe "#add_inventory" do
      it "adds the correct amount of inventory" do
        initial_product_inventory = @product[:inventory]
        vending_machine.add_inventory(@product_code, 12)
        expect(@product[:inventory]).to eq(initial_product_inventory + 12)
      end

      it "raises error when incorrect product code" do
        expect { vending_machine.add_inventory("incorrect_code", 12) }.to raise_error(IncorrectProductCodeError)
      end
    end

    describe "#remove_inventory" do
      it "removes the correct amount of inventory" do
        initial_product_inventory = @product[:inventory]
        vending_machine.remove_inventory(@product_code, 1)
        expect(@product[:inventory]).to eq(initial_product_inventory - 1)
      end

      it "raises error when incorrect product code" do
        expect { vending_machine.remove_inventory("incorrect_code", 12) }.to raise_error(IncorrectProductCodeError)
      end
    end

    describe "#add_funds" do
      it "calculates the correct balance" do
        initial_balance = vending_machine.balance
        adding_funds_amount = 1.25
        vending_machine.add_funds(adding_funds_amount)
        expect(vending_machine.balance).to eq(initial_balance + adding_funds_amount)
      end
    end

    describe "#withdraw_funds" do
      it "calculates the correct balance" do
        initial_balance = vending_machine.balance
        response = vending_machine.withdraw_funds
        expect(response[:message]).to eq("Dispensing $1.00")
        expect(vending_machine.balance).to eq(0.0)
      end
    end
  end
end
