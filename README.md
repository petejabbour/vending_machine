# Program a model of a vending machine. It should at least satisfy the following requirements:

* Contains various products, each of which can be retrieved by entering in the correct amount of money, and punching in the code for that item
* Dispenses change after making a purchase
* Has a cancel button that returns the user's money
* Ability to stock/restock items in the machine
* Any other functionality that you think a vending machine should have!

This is an exercise in domain modeling, and so it should not include any user interface or persistence.  Instead, the model's functionality should be demonstrated and verified with unit tests.  Write this code as if it were going into production.
